How run test_project
============
In terminal:

```
brew install python
```
then:
```
sudo pip install virtualenv
```
after that clone repo(need git):
```
git clone https://andrewosenenko@bitbucket.org/andrewosenenko/test_project.git
```

```
cd test_project/
```

create virtualenv:
```
virtualenv env
```

```
source env/bin/activate
```
add packages:
```
pip install django
```
```
pip install elementtree
```
run project:
```
python manage.py syncdb
```

```
python manage.py runserver
```
then go:
```
http://127.0.0.1:8000/
```
you need to have the access to the internet, because u can enter url for rss.