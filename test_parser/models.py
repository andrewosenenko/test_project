from django.db import models


class ParserData(models.Model):

    title = models.CharField(max_length=25)
    description = models.CharField(max_length=2000)