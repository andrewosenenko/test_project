# -*- coding: utf-8 -*-
import re
from django.shortcuts import render, redirect
from django.views.generic import View
import urllib
from test_parser.models import ParserData
from elementtree.ElementTree import parse


class ParserDataView(View):
    template = 'test_parser/main.html'

    def get(self, request):
        #get all parsed data(objects) from db for display
        return render(request, self.template,
                      {'parsed_data': ParserData.objects.all().order_by('pk').values('title', 'description')})

    def post(self, request):
        if request.POST['url']:
            # build tree of xml file and get root
            rss = parse(urllib.urlopen(request.POST['url'])).getroot()

            #parce data of tag "<channel></channel>" and create object(save into the db)
            for tag in rss.getiterator('channel'):
                ParserData.objects.get_or_create(title=tag.find('title').text,
                                                 description=re.sub('<[^>]*>', ' ', tag.find('description').text))

            #parce data of tags "<item></item>" and create object(save into the db)
            for tag in rss.getiterator('item'):
                ParserData.objects.get_or_create(title=tag.find('title').text,
                                                 description=re.sub('<[^>]*>', ' ', tag.find('description').text))
        return redirect('parse')