from django.conf.urls import patterns, include, url
from test_parser.views import ParserDataView
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', ParserDataView.as_view(), name='parse')
)
